export class MenuItemVm {
  Label: string;
  Link: string;

  constructor(label: string, link: string) {
    this.Label = label;
    this.Link = link;
  }
}
