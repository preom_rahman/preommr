import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _390685da = () => interopDefault(import('../src/pages/page1.vue' /* webpackChunkName: "pages/page1" */))
const _7931ab5e = () => interopDefault(import('../src/pages/misc/page1.vue' /* webpackChunkName: "pages/misc/page1" */))
const _fd7a633a = () => interopDefault(import('../src/pages/index.vue' /* webpackChunkName: "pages/index" */))
const _49a03c54 = () => interopDefault(import('../src/pages/index/hero.vue' /* webpackChunkName: "pages/index/hero" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
      path: "/page1",
      component: _390685da,
      name: "page1"
    }, {
      path: "/misc/page1",
      component: _7931ab5e,
      name: "misc-page1"
    }, {
      path: "/",
      component: _fd7a633a,
      name: "index",
      children: [{
        path: "hero",
        component: _49a03c54,
        name: "index-hero"
      }]
    }],

  fallback: false
}

export function createRouter() {
  return new Router(routerOptions)
}
